#!/bin/bash

# turn on bash's job control
set -m

# Start the primary process and put it in the background
/bin/sh -c /opt/mssql/bin/sqlservr &

# now we bring the primary process back into the foreground
# and leave it there
fg %1