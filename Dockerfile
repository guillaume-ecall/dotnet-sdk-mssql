FROM mcr.microsoft.com/mssql/server:2017-GA-ubuntu

RUN apt-get update && apt-get install -y --no-install-recommends \
		ca-certificates \
		curl \
		netbase \
		wget \
    apt-transport-https \
	&& rm -rf /var/lib/apt/lists/*


RUN set -ex; \
	if ! command -v gpg > /dev/null; then \
		apt-get update; \
		apt-get install -y --no-install-recommends \
			gnupg \
			dirmngr \
		; \
		rm -rf /var/lib/apt/lists/*; \
	fi
  
# Install .NET Core SDK
ENV DOTNET_SDK_VERSION 2.2
  
RUN wget -q https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb \
  && dpkg -i packages-microsoft-prod.deb \
	&& rm -f packages-microsoft-prod.deb
 
RUN apt-get update && apt-get install -y dotnet-sdk-2.2 \
	&& rm -rf /var/lib/apt/lists/*

# Configure web servers to bind to port 80 when present
ENV ASPNETCORE_URLS=http://+:80 \
    # Enable detection of running in a container
    DOTNET_RUNNING_IN_CONTAINER=true \
    # Enable correct mode for dotnet watch (only mode supported in a container)
    DOTNET_USE_POLLING_FILE_WATCHER=true \
    # Skip extraction of XML docs - generally not useful within an image/container - helps performance
    NUGET_XMLDOC_MODE=skip
    
# Trigger first run experience by running arbitrary cmd to populate local package cache
RUN dotnet help

COPY entrypoint.sh entrypoint.sh
CMD ./entrypoint.sh